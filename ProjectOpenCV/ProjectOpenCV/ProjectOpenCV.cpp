/* ProjectOpenCV.cpp : This file contains the 'main' function. Program execution begins and ends there.

Polsko-Japonska Akademia Technik Komputerowych w Gdansku
"Narzedzia Sztucznej Inteligencji", dr Tadeusz Puzniakowski
Zrodla:
-"Introduction to OpenCV > Installation in Windows" https://docs.opencv.org/4.0.1/d3/d52/tutorial_windows_install.html
-"Introduction to OpenCV > How to build applications with OpenCV inside the "Microsoft Visual Studio" https://docs.opencv.org/4.0.1/dd/d6e/tutorial_windows_visual_studio_opencv.html
-"Creating a Cascade of Haar-Like Classifiers: Step by Step" https://docplayer.net/30654278-Creating-a-cascade-of-haar-like-classifiers-step-by-step.html
-"Read, Write and Display a video using OpenCV ( C++/ Python )" https://www.learnopencv.com/read-write-and-display-a-video-using-opencv-cpp-python/
-"Glasses, dr Tadeusz Puzniakowski" -> https://github.com/pantadeusz/examples-ai/blob/master/opencv/glasses/glasses.cpp
- Video file "sample.mp4" (00:00-02:00) -> https://www.youtube.com/watch?v=nt3D26lrkho
*/

#include "pch.h"
#include <iostream>
#include <opencv2/objdetect.hpp> 
#include <opencv2/highgui.hpp> 
#include <opencv2/imgproc.hpp>

using namespace std;
using namespace cv;

void drawVehicles(Mat & frame, vector< Rect > & detectedCars, Scalar color, int thickness);
int countVehicles( vector< Rect > & detectedCars);

const Scalar RED (0, 0, 255);
const Scalar GREEN (0, 255, 0);
const int VIDEO_WIDTH = 854;
const int VIDEO_HEIGHT = 480;
const int THICKNESS = 2;

int main()
{
	VideoCapture cap("sample2.mp4");
	CascadeClassifier cascade;
	vector<Rect> detectedCars;
	short int fps = 1;
	int vehiclesCounter = 0;
	bool continueCapture = true;

	cascade.load("cars2.xml");
	if (!cap.isOpened()) {
		cout << "ERROR - Can't open video file.";
		return -1;
	}
	while (continueCapture) {
		Mat frame, gray;
		if (cap.read(frame)) {
			cvtColor(frame, gray, COLOR_RGB2GRAY);
			GaussianBlur(gray, gray, Size(3, 3),0,0);
			equalizeHist(gray, gray);
			if (fps % 5 == 0) {
				cascade.detectMultiScale(gray, detectedCars, 1.1, 3, 0 | CASCADE_SCALE_IMAGE, Size(64, 64));
				vehiclesCounter += countVehicles(detectedCars);
				fps = 0;
			}
			drawVehicles(frame, detectedCars, GREEN, THICKNESS);
			line(frame,Point(1,(int)VIDEO_HEIGHT/2),Point(VIDEO_WIDTH, (int)VIDEO_HEIGHT / 2), RED, THICKNESS*2);
			putText(frame, to_string(vehiclesCounter)+","+to_string(detectedCars.size()), Point(10,60), 0, 2, RED, THICKNESS*2);
			imshow("NAI - s13144 & s14329", frame);
			fps++;
		} else continueCapture = false;
		if ((waitKey(15) & 0x0ff) == 27 ) continueCapture = false;
	}
	cap.release();
	destroyAllWindows();
	return 0;
}

void drawVehicles(Mat & frame, vector< Rect > & detectedCars, Scalar color, int thickness) {
	if (!detectedCars.empty())
	{
		for (int i = 0; i < detectedCars.size(); i++) {
			rectangle(frame,detectedCars[i], color, thickness);
		}
	}
}

int countVehicles( vector< Rect > & detectedCars) {
	int counter = 0;
	if (!detectedCars.empty()) {
		for (int i=0; i< detectedCars.size(); i++)
		{
			if (detectedCars[i].y <= (int)VIDEO_HEIGHT / 2 + 3 && detectedCars[i].y > (int)VIDEO_HEIGHT / 2 - 3) counter++;
		}
	}
	return counter;
}